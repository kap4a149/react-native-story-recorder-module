import { NativeModules } from 'react-native'

const { CampusStoryAndroid } = NativeModules

export default {
  exampleMethod () {
    return CampusStoryAndroid.exampleMethod()
  },

  EXAMPLE_CONSTANT: CampusStoryAndroid.EXAMPLE_CONSTANT
}
