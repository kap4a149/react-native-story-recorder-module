package com.example.androiduloopcameraapp;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import static com.example.androiduloopcameraapp.CampusStoryAndroidModule.reactBridgeTransfer;

public class ActivityPicture extends AppCompatActivity {

    private ImageView imageView;
    private ImageButton saveImageButton, sendStoryBtn;
    private static final String IMAGE_DIRECTORY = "/CustomImage";
    private int rotate = 90;
    private boolean isCameraFront = false;
    private LinearLayout pictureBottomLayout;
    private EditText captionTextInput;
    private Camera mCamera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle bundle = getIntent().getExtras();
        isCameraFront = bundle.getBoolean("isCameraFront", false);

        if(isCameraFront == true) {
            rotate = 270;
        }



        this.initializeCamLayoutSettings();
    }


    public void initializeCamLayoutSettings() {
        setContentView(R.layout.activity_picture);


        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        imageView = findViewById(R.id.img);
        imageView.setImageBitmap(this.rotateBitmapImage(MainActivity.bitmap));
        captionTextInput = findViewById(R.id.captionText);
        sendStoryBtn = (ImageButton) findViewById(R.id.button2);
        LinearLayout pictureBottomLayout = findViewById(R.id.pictureBottomLayout);
        saveImageButton = (ImageButton) findViewById(R.id.saveMedia);



        captionTextInput.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (null != captionTextInput.getLayout() && captionTextInput.getLayout().getLineCount() > 2) {
                    captionTextInput.getText().delete(captionTextInput.getText().length() - 1, captionTextInput.getText().length());
                }
            }
        });


        saveImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    saveImageButton.setBackgroundResource(R.drawable.saving_circle);
                    Bitmap preparedBitmap = cropBitmapImage(MainActivity.bitmap);
                    saveImage(preparedBitmap);
//                    saveImageButton.setBackgroundResource(R.drawable.saving_mark);
                }
                catch (Exception e){}
//                    saveImageButton.setBackgroundResource(R.drawable.saving_mark);

            }
        });


        sendStoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.e("TESTTTTTT", "#333333");
                    reactBridgeTransfer();
//                  returnMediaDataToReact();
//                    CampusStoryAndroidModule campusStoryAndroidModule = new CampusStoryAndroidModule();
                }
                catch (Exception e){}
//                    saveImageButton.setBackgroundResource(R.drawable.saving_mark);

            }
        });


        Display display = getWindowManager().getDefaultDisplay();
        int pictureBottomLayoutHeight = display.getHeight()/3;
        pictureBottomLayout.getLayoutParams().height = pictureBottomLayoutHeight ; // aspect ratio 3:2




    }



    public String saveImage(Bitmap myBitmap) {
        String inputText = captionTextInput.getText().toString();

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        Resources resources = getApplicationContext().getResources();
        float scale = resources.getDisplayMetrics().density;
        Canvas canvas = new Canvas(myBitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.rgb(110,110, 110));
        // text size in pixels
        paint.setTextSize((int) (48 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY);

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(inputText, 0, inputText.length(), bounds);
        int x = (myBitmap.getWidth() - bounds.width())/6;
        int y = (myBitmap.getHeight() + bounds.height())/5;

        canvas.drawText(inputText, x * scale, y * scale, paint);



        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.

        if (!wallpaperDirectory.exists()) {
            Log.d("dirrrrrr", "" + wallpaperDirectory.mkdirs());
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();   //give read write permission
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";

    }


    public Bitmap cropBitmapImage(Bitmap myBitmap){


        Matrix matrix = new Matrix();
        int bitmapWidth     = myBitmap.getWidth();
        int bitmapheight    = myBitmap.getHeight();
        int cropWidth;
        int cropHeight;

        if(!isCameraFront){
            cropWidth  = (int) (bitmapWidth * 0.75);
            cropHeight = (int) (bitmapWidth * 0.5);
        }
        else{
            cropWidth  = bitmapheight;
            cropHeight = (int) (bitmapWidth * 0.5);
        }


        // make sure crop isn't larger than bitmap size
        cropWidth   = (cropWidth > bitmapWidth) ? bitmapWidth : cropWidth;
        cropHeight  = (cropHeight > bitmapheight) ? bitmapheight : cropHeight;

        int newX = bitmapWidth/2  - cropWidth/2;
        int newY = bitmapheight/2 - cropHeight/2;


        if (!isCameraFront) {
            matrix.postRotate(90);
            myBitmap = Bitmap.createBitmap(myBitmap,0, newY, cropWidth - 300, cropHeight, matrix, false);
        }
        else {
            matrix.postScale(1, -1, cropWidth/2, cropHeight/2);
            matrix.postRotate(270);
            myBitmap = Bitmap.createBitmap(myBitmap, newX, 0, cropWidth, cropHeight, matrix, false);
        }

        return myBitmap;
    }

    public Bitmap rotateBitmapImage(Bitmap myBitmap){
        Matrix matrix = new Matrix();

        if(isCameraFront){
            matrix.postScale(1, -1, myBitmap.getWidth()/2, myBitmap.getHeight()/2);
        }

        matrix.postRotate(rotate);
        myBitmap = Bitmap.createBitmap(myBitmap,0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, false);

        return myBitmap;
    }

    public String[] returnMediaDataToReact(){
        Bitmap preparedBitmap = cropBitmapImage(MainActivity.bitmap);
        String result = MediaStore.Images.Media.insertImage(getContentResolver(), preparedBitmap, "", "");
        String[] data = new String[2];
        data[0] = String.valueOf(Uri.parse(result));
        data[1] = captionTextInput.getText().toString();

        return data;
    }


}