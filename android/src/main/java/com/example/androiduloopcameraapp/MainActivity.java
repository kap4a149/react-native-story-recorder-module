package com.example.androiduloopcameraapp;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import static android.hardware.Camera.Parameters.FOCUS_MODE_AUTO;

public class MainActivity extends AppCompatActivity {

    private Camera mCamera;
    private MediaRecorder mediaRecorder;
    private Camera.Parameters mParams;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private ImageButton switchCamera, capture, toggleFlash;
    private TextView videoTimer;
    private Context myContext;
    private LinearLayout cameraPreview, centerBottomLayout, videoTimerLayout;
    private int cameraId;
    private boolean cameraFront;
    private boolean enabledFlashLight = false;
    public static Bitmap bitmap;
    CountDownTimer countDownTimer;
    File videoFile;

    private static  final int FOCUS_AREA_SIZE = 300;

    public boolean isCameraFront() {
        return cameraFront;
    }

    public void setCameraFront(boolean cameraFront) {
        this.cameraFront = cameraFront;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        File pictures = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        videoFile = new File(pictures, "myvideo.mp4");
        Log.d("videooopath", "" + videoFile);

        try{
            this.checkNeedlePermissions();
            this.initializeCamLayoutSettings();
            this.initializeCamera();
            this.checkAvaliableFlash();
            this.setCameraDisplayOrientation();
        }
        catch(Exception e ){

            Log.d("TestLog122:", "" + e);
        }
        mCamera.startPreview();

    }




    public void initializeCamLayoutSettings(){

        setContentView(R.layout.activity_main);

//        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        myContext = getApplicationContext();
        cameraPreview = (LinearLayout) findViewById(R.id.cPreview);
        centerBottomLayout = (LinearLayout) findViewById(R.id.centerBottomLayout);
        capture = (ImageButton) findViewById(R.id.btnCam);
        switchCamera = (ImageButton) findViewById(R.id.btnSwitchCam);
        videoTimer = (TextView) findViewById(R.id.videoTimer);
        videoTimerLayout = (LinearLayout) findViewById(R.id.videoTimerLayout);



        Display display = getWindowManager().getDefaultDisplay();
        int centerBottomLayoutHeight = display.getHeight()/3;
        centerBottomLayout.getLayoutParams().height = centerBottomLayoutHeight ; // aspect ratio 3:2

        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);


        final GestureDetector gd = new GestureDetector(myContext, new GestureDetector.SimpleOnGestureListener(){

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if(mediaRecorder != null){
                    return false;
                }

                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    releaseCamera();
                    chooseCamera();
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                focusOnTouch(e);
                return super.onSingleTapConfirmed(e);
            }
        });

        final GestureDetector gd1 = new GestureDetector(myContext, new GestureDetector.SimpleOnGestureListener(){

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                if (prepareVideoRecorder()) {
                    Log.d("4555556", "lock");

                    mediaRecorder.start();

                } else {
                    Log.d("4555556", "unlock");
                    releaseMediaRecorder();
                }
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                    releaseMediaRecorder();
                }
                else{
                    mCamera.takePicture(null, null, mPicture);
                }
                return super.onSingleTapConfirmed(e);
            }
        });



        cameraPreview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gd.onTouchEvent(event);
            }
        });


        capture.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gd1.onTouchEvent(event);
            }
        });

//        capture.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                prepareVideoRecorder();
//                    mediaRecorder.start();
//                mCamera.takePicture(null, null, mPicture);
//            }
//        });

        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the number of cameras
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    releaseCamera();
                    chooseCamera();
                }
            }
        });

        toggleFlash = (ImageButton) findViewById(R.id.toggleFlash);
        toggleFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enabledFlashLight = !enabledFlashLight;
                cameraFlashMode(enabledFlashLight);
            }
        });
    }

    private void focusOnTouch(MotionEvent event) {

        List<String> supportedFocusModes = mCamera.getParameters().getSupportedFocusModes();
        boolean hasAutoFocus = supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO);

        if(hasAutoFocus == false){
            return;
        }


        if (mCamera != null ) {

            if (mParams.getMaxNumMeteringAreas() > 0){
                Rect rect = calculateFocusArea(event.getX(), event.getY());
                mParams.setFocusMode(FOCUS_MODE_AUTO);
//                List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
//                meteringAreas.add(new Camera.Area(rect, 800));
//                mParams.setFocusAreas(meteringAreas);

                mCamera.setParameters(mParams);
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            }else {
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            }
        }
    }

    private Rect calculateFocusArea(float x, float y) {
        int left = clamp(Float.valueOf((x / cameraPreview.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
        int top = clamp(Float.valueOf((y / cameraPreview.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

        return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
            if (touchCoordinateInCameraReper>0){
                result = 1000 - focusAreaSize/2;
            } else {
                result = -1000 + focusAreaSize/2;
            }
        } else{
            result = touchCoordinateInCameraReper - focusAreaSize/2;
        }
        return result;
    }



    private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (success) {
                // do something...
                Log.i("tap_to_focus","success!");
            } else {
                // do something...
                Log.i("tap_to_focus","fail!");
            }
        }
    };





    public void initializeCamera(){
        cameraId = findBackFacingCamera();
        if (cameraId >= 0) {
            mCamera = Camera.open(cameraId);

            Camera.Parameters params = mCamera.getParameters();
            if (params.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                mParams = params;
            }

            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
            this.cameraFlashMode(enabledFlashLight);

            mCamera.setParameters(params);
        }
    }





    //Some manufacturers does not support FLASH_MODE_TORCH
    public void cameraFlashMode(Boolean enabledFlashLight) {
        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFlashMode() == null) {
            return;
        }

        if(enabledFlashLight) {
            if (mCamera != null) {
                if (mParams != null) {
                    List<String> supportedFlashModes = mParams.getSupportedFlashModes();

                    if (supportedFlashModes != null) {

                        if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_TORCH)) {
                            mParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        } else if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
                            mParams.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        } else mCamera = null;
                    } else Log.d("Camera flash: ", "Camera is null.");
                }
            }
        }
        else {
            mParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        }
        mCamera.setParameters(mParams);
    }



    private void checkNeedlePermissions(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CAMERA}, 50);
        }

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 60);
        }

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, 70);
        }
    }

    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
//                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
//                cameraFront = false;
                break;
            }
        }
        return cameraId;
    }

    public void onResume() {

        super.onResume();
        if(mCamera == null) {
            mCamera = Camera.open();
            this.setCameraDisplayOrientation();
            mCamera.setParameters(mParams);
            this.setCameraFront(false);
            this.checkAvaliableFlash();
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
        }

    }

    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                this.setCameraFront(false);
//                cameraFront = false;
                mCamera = Camera.open(cameraId);
                this.setCameraDisplayOrientation();
                mCamera.setParameters(mParams);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                this.setCameraFront(true);
//                cameraFront = true;
                mCamera = Camera.open(cameraId);
                this.setCameraDisplayOrientation();
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        }
        this.checkAvaliableFlash();
    }

    public void checkAvaliableFlash(){
        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFlashMode() == null) {
            toggleFlash.getBackground().setAlpha(128);
        }
        else{
            toggleFlash.getBackground().setAlpha(255);
        }
    }


    public void setCameraDisplayOrientation() {
        Camera.CameraInfo camInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, camInfo);

        Display display = ((WindowManager) myContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        int rotation = display.getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;

        if (camInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (camInfo.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (camInfo.orientation - degrees + 360) % 360;
        }

        mCamera.setDisplayOrientation(result);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        releaseCamera();
    }

    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                Intent intent = new Intent(MainActivity.this,ActivityPicture.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isCameraFront", isCameraFront());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        };
        return picture;
    }


    private boolean prepareVideoRecorder() {
        releaseMediaRecorder();

        mCamera.unlock();

        mediaRecorder = new MediaRecorder();

        mediaRecorder.setCamera(mCamera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setMaxDuration(15000);
        mediaRecorder.setOrientationHint(90);

        if(cameraFront){
            mediaRecorder.setOrientationHint(270);
        }

        mediaRecorder.setProfile(CamcorderProfile
                .get(CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setVideoEncodingBitRate(1000000);
        mediaRecorder.setOutputFile(videoFile.getAbsolutePath());

        toggleFlash.setVisibility(View.INVISIBLE);
        switchCamera.setVisibility(View.INVISIBLE);
        videoTimerLayout.setVisibility(View.VISIBLE);
        capture.setBackgroundResource(R.drawable.take_photo_red_icon);


        countDownTimer = new CountDownTimer(16000, 1000) {
            public void onTick(long millisUntilFinished) {
                videoTimer.setText("0:" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                countDownTimer.cancel();
            }
        }.start();

        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener()
        {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra)
            {
                if(what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED)
                {
                    releaseMediaRecorder();
                }
            }
        });


        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            mCamera.lock();
            toggleFlash.setVisibility(View.VISIBLE);
            switchCamera.setVisibility(View.VISIBLE);
            videoTimerLayout.setVisibility(View.INVISIBLE);
            capture.setBackgroundResource(R.drawable.take_photoicon);
            countDownTimer.cancel();

//            Intent intent = new Intent(MainActivity.this,ActivityPicture.class);
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("isCameraFront", isCameraFront());
//            intent.putExtras(bundle);
//            startActivity(intent);

        }
    }
}
